const validate_is_number_inputs = require('./validate_is_number_inputs');

test('check valid input (error object param 2)', () => {
  expect(() => {
    validate_is_number_inputs(1, new Error('fail'));
  }).toThrow('invalid input');
});

test('check valid input (error object param 1)', () => {
  expect(() => {
    validate_is_number_inputs(new Error('fail'), 1);
  }).toThrow('invalid input');
});

test('check valid input (string param 2)', () => {
  expect(() => {
    validate_is_number_inputs(1, '4');
  }).toThrow('input need to be number');
});

test('check valid input (string param 1)', () => {
  expect(() => {
    validate_is_number_inputs('4', 1);
  }).toThrow('input need to be number');
});

test('function as param', () => {
  expect(() => {
    validate_is_number_inputs(1, () => 5);
  }).toThrow('invalid input');
});
