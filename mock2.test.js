describe('mocking', () => {
  test('.toBeUndefined', () => {
    const myMock = jest.fn();

    expect(myMock()).toBeUndefined();
  });

  test('define list of values', () => {
    const myMock = jest.fn();

    myMock
      .mockReturnValueOnce(10) // first return value
      .mockReturnValueOnce('x') // 2nd return value
      .mockReturnValue(true); // 3rd and all after that return values

    expect(myMock(3, 4, 'x')).toBe(10);
    expect(myMock(5, '24', 'U')).toBe('x');
    expect(myMock(7)).toBe(true);
    expect(myMock(1, '234', 'g')).toBe(true);

    expect(myMock.mock.calls).toHaveLength(4);

    expect(myMock.mock.calls[2][0]).toBe(7);
    expect(myMock.mock.calls[1][2]).toBe('U');

    expect(myMock.mock.results[1].value).toBe('x');
  });
});
